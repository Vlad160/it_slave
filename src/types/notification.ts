export enum LogLevel {
  VERBOSE,
  INFO,
  ERROR,
  WARNING,
}
