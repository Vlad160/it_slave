export interface EventDetails {
  title: string;
  description: string;
  submitText: string;
  denyText: string;
}

export enum Priority {
  SPECIAL = 0,
  NONE = SPECIAL,
  VERY_LOW = 1,
  LOW = 2,
  ABOVE_LOW = 3,
  BELOW_MEDIUM = 4,
  MEDIUM = 5,
  ABOVE_MEDIUM = 6,
  BELOW_HIGH = 7,
  HIGH = 8,
  VERY_HIGH = 9,
  ADMIRAL_BUBA = 10,
}

export enum Progress {
  IN_PROGRESS,
  FINISHED,
}

export interface Role {
  id: string;
  title: string;
  amount: number;
  status: Progress;
}

export class PaddleEvent {
  private timerId: number;
  private disabled: boolean = false;
  private priority: Priority;
  constructor(
    public readonly name: string,
    priority: Priority = Priority.BELOW_MEDIUM,
    public readonly details: EventDetails,
    disabled = false
  ) {
    this.priority = priority;
    this.disabled = disabled;
  }

  disable(time?: number) {
    this.disabled = true;
    if (typeof time !== 'undefined') {
      this.timerId = (setTimeout(
        () => (this.disabled = false),
        time
      ) as unknown) as number;
    }
  }

  enable() {
    this.disabled = false;
    if (this.timerId) {
      clearTimeout(this.timerId);
    }
  }

  setPriority(priority: Priority) {
    this.priority = priority;
  }

  addPriority(priority: Priority) {
    this.priority = Math.min(priority + this.priority, Priority.VERY_HIGH);
  }

  isDisabled(): boolean {
    return this.disabled;
  }

  getPriority(): Priority {
    return this.priority;
  }
}
