import { Action } from 'redux';

export enum ManagerProtectsActionTypes {
  MANAGER_PROTECTS_STARTED = '[Manager protects event] Manager protects event started',
  MANAGER_PROTECTS_ENDED = '[Manager protects event] Manager protects event event ended',
}

export class ManagerProtectsStarted implements Action {
  readonly type = ManagerProtectsActionTypes.MANAGER_PROTECTS_STARTED;
}

export class ManagerProtectsEnded implements Action {
  readonly type = ManagerProtectsActionTypes.MANAGER_PROTECTS_ENDED;
}

export type ManagerProtectsActions =
  | ManagerProtectsStarted
  | ManagerProtectsEnded;
