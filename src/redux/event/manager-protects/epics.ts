import { PaddleEvent, Priority } from '../../../types';
import { EndEvent, EventActions, EventNames, OpenEventBox } from '../actions';
import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import { Observable } from 'rxjs';
import { ofEventType } from '../../redux-helper';
import { mapTo, switchMap } from 'rxjs/operators';
import {
  ManagerProtectsActions,
  ManagerProtectsActionTypes,
  ManagerProtectsEnded,
  ManagerProtectsStarted,
} from './actions';
import { waitForAnswer } from '../operators';
import { AddIncome, CommonActions } from '../../common';

export const managerProtectsEvent = new PaddleEvent(
  EventNames.MANAGER_PROTECTS,
  Priority.SPECIAL,
  {
    title: 'Погонщик на вашей стороне',
    denyText: null,
    description:
      'Погонщик быстро сообразил кто тут Chief Architect по кражам ссобоек и устроил разнос воришке. Вы ликуете, есть еще место для справедливости на галере.\n',
    submitText: 'Да, сука, будешь знать как мою ссобойку красть! Ныа!',
  }
);

const onManagerProtectsEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<ManagerProtectsActions> =>
  action$.pipe(
    ofEventType(EventNames.MANAGER_PROTECTS),
    mapTo(new ManagerProtectsStarted())
  );

const managerProtectsStarted$ = (
  action$: ActionsObservable<ManagerProtectsActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(ManagerProtectsActionTypes.MANAGER_PROTECTS_STARTED),
    mapTo(new OpenEventBox(managerProtectsEvent.details))
  );

const managerProtectsEvent$ = (
  action$: ActionsObservable<ManagerProtectsActions>
): Observable<CommonActions | ManagerProtectsActions> =>
  action$.pipe(
    ofType(ManagerProtectsActionTypes.MANAGER_PROTECTS_STARTED),
    switchMap(() => waitForAnswer(action$)),
    switchMap(() => [new AddIncome({ value: 300 }), new ManagerProtectsEnded()])
  );

const managerProtectsEventEnded$ = (
  action$: ActionsObservable<ManagerProtectsActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(ManagerProtectsActionTypes.MANAGER_PROTECTS_ENDED),
    mapTo(new EndEvent())
  );

export const managerProtectsEpic$ = combineEpics(
  onManagerProtectsEvent$,
  managerProtectsStarted$,
  managerProtectsEvent$,
  managerProtectsEventEnded$
);
