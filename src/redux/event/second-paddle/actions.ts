import { Action } from 'redux';

export enum SecondPaddleActionTypes {
  SECOND_PADDLE_STARTED = '[Second paddle event] Second paddle started',
  SECOND_PADDLE_ENDED = '[Second paddle event] Second paddle ended',
  CHANGE_SECOND_PRIORITY = '[Second paddle event] Change priority',
}

export class SecondPaddleStarted implements Action {
  readonly type = SecondPaddleActionTypes.SECOND_PADDLE_STARTED;
}

export class SecondPaddleEnded implements Action {
  readonly type = SecondPaddleActionTypes.SECOND_PADDLE_ENDED;
}

export class ChangePriority implements Action {
  readonly type = SecondPaddleActionTypes.CHANGE_SECOND_PRIORITY;
}

export type SecondPaddleActions =
  | SecondPaddleStarted
  | SecondPaddleEnded
  | ChangePriority;
