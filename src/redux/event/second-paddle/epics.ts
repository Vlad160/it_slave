import { PaddleEvent, Priority } from '../../../types';
import {
  EndEvent,
  EventActions,
  EventNames,
  OpenEventBox,
  SetPriority,
} from '../actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { Observable } from 'rxjs';
import { ofEventType } from '../../redux-helper';
import {
  distinctUntilChanged,
  filter,
  map,
  mapTo,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  SecondPaddleActions,
  SecondPaddleActionTypes,
  SecondPaddleEnded,
  SecondPaddleStarted,
} from './actions';
import {
  AddPassiveIncome,
  CommonActions,
  CommonActionTypes,
} from '../../common';
import { AppState } from '../../index';
import { SENIOR_SOFTWARE_GREBEC_ENGINEER } from '../../../constants';
import { getGameStarted, getIncome, getRoles } from '../../common/selectors';
import { deepEqual } from '../../../utils';
import { waitForAnswer } from '../operators';

export const secondPaddleEvent = new PaddleEvent(
  EventNames.SECOND_PADDLE,
  Priority.SPECIAL,
  {
    title: 'Второе весло',
    denyText: null,
    description:
      'Погонщик считает что вы можете справляться с двумя вёслами одновременно. Гребите во славу компании!\n',
    submitText: 'Ура! Я рад делать что-то хорошее для других людей!',
  }
);

const MIN_ROLE = SENIOR_SOFTWARE_GREBEC_ENGINEER;

const onSecondPaddleEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<SecondPaddleActions> =>
  action$.pipe(
    ofEventType(EventNames.SECOND_PADDLE),
    mapTo(new SecondPaddleStarted())
  );

const secondPaddleEventStarted$ = (
  action$: ActionsObservable<SecondPaddleActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(SecondPaddleActionTypes.SECOND_PADDLE_STARTED),
    mapTo(new OpenEventBox(secondPaddleEvent.details))
  );

const changePriority$ = (
  action$: ActionsObservable<CommonActions>,
  state$: StateObservable<AppState>
): Observable<EventActions | SecondPaddleActions> =>
  action$.pipe(
    ofType(CommonActionTypes.SET_ROLE),
    withLatestFrom(state$),
    filter(([, state]) => getGameStarted(state)),
    withLatestFrom(state$),
    map(([, state]) => {
      const minRole = getRoles(state).find(role => role.title === MIN_ROLE);
      if (!minRole) {
        return null;
      }
      const currentIncome = getIncome(state);
      const priority =
        currentIncome >= minRole.amount ? Priority.MEDIUM : Priority.NONE;
      return new SetPriority({
        name: EventNames.SECOND_PADDLE,
        priority,
      });
    }),
    filter<SetPriority>(Boolean),
    distinctUntilChanged(deepEqual)
  );

const secondPaddleEvent$ = (
  action$: ActionsObservable<SecondPaddleActions>
): Observable<EventActions | SecondPaddleActions> =>
  action$.pipe(
    ofType(SecondPaddleActionTypes.SECOND_PADDLE_STARTED),
    switchMap(() => waitForAnswer(action$)),
    map(() => new AddPassiveIncome({ value: 1 })),
    switchMap((action: any) => [action, new SecondPaddleEnded()])
  );

const secondPaddleEventEnded$ = (
  action$: ActionsObservable<SecondPaddleActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(SecondPaddleActionTypes.SECOND_PADDLE_ENDED),
    mapTo(new EndEvent())
  );

export const secondPaddleEpic$ = combineEpics(
  onSecondPaddleEvent$,
  changePriority$,
  secondPaddleEventStarted$,
  secondPaddleEvent$,
  secondPaddleEventEnded$
);
