import { PaddleEvent } from '../../types';
import { smokeBreakEvent } from './smoke-break/epics';
import { demotedEvent } from './demoted/epics';
import { firedEvent } from './fired/epic';
import { managerProtectsEvent } from './manager-protects/epics';
import { noChanceEvent } from './no-chance/epics';
import { suddenMeetingEvent } from './sudden-meeting/epics';
import { beerEvent } from './beer/epics';
import { foodStolenEvent } from './food-stolen/epics';
import { friendlyManagerEvent } from './friendly-manager/epics';
import { secondPaddleEvent } from './second-paddle/epics';
import { suddenManager } from './sudden-manager/epics';

export const events: PaddleEvent[] = [
  smokeBreakEvent,
  demotedEvent,
  firedEvent,
  managerProtectsEvent,
  noChanceEvent,
  suddenMeetingEvent,
  beerEvent,
  foodStolenEvent,
  friendlyManagerEvent,
  secondPaddleEvent,
  suddenManager,
];
