import { Action } from 'redux';

export enum SmokeBreakActionTypes {
  SMOKE_BREAK_STARTED = '[Smoke break event] Smoke break started',
  SMOKE_BREAK_ENDED = '[Smoke break event] Smoke break ended',
}

export class SmokeBreakStarted implements Action {
  readonly type = SmokeBreakActionTypes.SMOKE_BREAK_STARTED;
}

export class SmokeBreakEnded implements Action {
  readonly type = SmokeBreakActionTypes.SMOKE_BREAK_ENDED;
}

export type SmokeBreakActions = SmokeBreakStarted | SmokeBreakEnded;
