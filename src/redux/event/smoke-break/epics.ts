import { PaddleEvent, Priority } from '../../../types';
import {
  AddPriority,
  EndEvent,
  EventActions,
  EventNames,
  OpenEventBox,
} from '../actions';
import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import { Observable } from 'rxjs';
import { AddPassiveIncome, CommonActions } from '../../common';
import { map, mapTo, switchMap } from 'rxjs/operators';
import { ofEventType } from '../../redux-helper';
import {
  SmokeBreakActions,
  SmokeBreakActionTypes,
  SmokeBreakEnded,
  SmokeBreakStarted,
} from './actions';
import { waitForAnswer } from '../operators';

export const smokeBreakEvent = new PaddleEvent(
  EventNames.SMOKE_BREAK,
  Priority.MEDIUM,
  {
    title: 'Перекур, работяги',
    denyText: 'Нет, лучше налягу с двойной силой',
    description: 'Коллеги предлагают сходить на перекур',
    submitText: 'Согласиться, не все же время грести',
  }
);

const onSmokeBreakEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<SmokeBreakActions> =>
  action$.pipe(
    ofEventType(EventNames.SMOKE_BREAK),
    mapTo(new SmokeBreakStarted())
  );

const smokeBreakEventStarted$ = (
  action$: ActionsObservable<SmokeBreakActions>
): Observable<EventActions | CommonActions> =>
  action$.pipe(
    ofType(SmokeBreakActionTypes.SMOKE_BREAK_STARTED),
    mapTo(new OpenEventBox(smokeBreakEvent.details))
  );

export const smokeBreakEvent$ = (
  action$: ActionsObservable<EventActions | SmokeBreakActions>
): Observable<SmokeBreakActions | CommonActions | EventActions> =>
  action$.pipe(
    ofType(SmokeBreakActionTypes.SMOKE_BREAK_STARTED),
    switchMap(() => waitForAnswer(action$)),
    map(result =>
      result
        ? new AddPriority({
            name: EventNames.MANAGER_PROTECTS,
            priority: Priority.VERY_LOW,
          })
        : new AddPassiveIncome({ value: 1, time: 100 * 1000 })
    ),
    switchMap(action => [action, new SmokeBreakEnded()])
  );

export const smokeBreakEventEnded$ = (
  action$: ActionsObservable<EventActions | SmokeBreakActions>
): Observable<SmokeBreakActions | CommonActions | EventActions> =>
  action$.pipe(
    ofType(SmokeBreakActionTypes.SMOKE_BREAK_ENDED),
    mapTo(new EndEvent())
  );

export const smokeBreakEpic$ = combineEpics(
  onSmokeBreakEvent$,
  smokeBreakEventStarted$,
  smokeBreakEvent$,
  smokeBreakEventEnded$
);
