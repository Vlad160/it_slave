import { Action } from 'redux';

export enum SuddenMeetingActionTypes {
  SUDDEN_MEETING_STARTED = '[Sudden meeting event] Sudden meeting started',
  SUDDEN_MEETING_ENDED = '[Sudden meeting event] Sudden meeting ended',
}

export class SuddenMeetingStarted implements Action {
  readonly type = SuddenMeetingActionTypes.SUDDEN_MEETING_STARTED;
}

export class SuddenMeetingEnded implements Action {
  readonly type = SuddenMeetingActionTypes.SUDDEN_MEETING_ENDED;
}

export type SuddenMeetingActions = SuddenMeetingStarted | SuddenMeetingEnded;
