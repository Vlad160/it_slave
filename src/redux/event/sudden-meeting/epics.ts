import { PaddleEvent, Priority } from '../../../types';
import { EndEvent, EventActions, EventNames, OpenEventBox } from '../actions';
import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import { Observable, timer } from 'rxjs';
import { CommonActions, DisablePaddle, EnablePaddle } from '../../common';
import { ofEventType } from '../../redux-helper';
import { mapTo, switchMap } from 'rxjs/operators';
import {
  SuddenMeetingActions,
  SuddenMeetingActionTypes,
  SuddenMeetingEnded,
  SuddenMeetingStarted,
} from './actions';

export const suddenMeetingEvent = new PaddleEvent(
  EventNames.SUDDEN_MEETING,
  Priority.MEDIUM,
  {
    title: 'Внезапный митинг',
    denyText: null,
    description:
      'Кастомер решил сказать о том как ему важна ваша работа и как это сильно влияет на общий успех компании',
    submitText: null,
  }
);

const DISABLE_PADDLE_TIME = 30000;

const suddenMeetingStarted$ = (
  action$: ActionsObservable<EventActions>
): Observable<SuddenMeetingActions> =>
  action$.pipe(
    ofEventType(EventNames.SUDDEN_MEETING),
    mapTo(new SuddenMeetingStarted())
  );

const suddenMeetingEvent$ = (
  action$: ActionsObservable<SuddenMeetingActions>
): Observable<EventActions | CommonActions> =>
  action$.pipe(
    ofType(SuddenMeetingActionTypes.SUDDEN_MEETING_STARTED),
    switchMap(() => [
      new OpenEventBox(suddenMeetingEvent.details),
      new DisablePaddle(),
    ])
  );

const suddenMeetingTimer$ = (
  action$: ActionsObservable<SuddenMeetingActions>
): Observable<SuddenMeetingActions> =>
  action$.pipe(
    ofType(SuddenMeetingActionTypes.SUDDEN_MEETING_STARTED),
    switchMap(() =>
      timer(DISABLE_PADDLE_TIME).pipe(mapTo(new SuddenMeetingEnded()))
    )
  );

const suddenMeetingEnded$ = (
  action$: ActionsObservable<SuddenMeetingActions>
): Observable<EventActions | CommonActions> =>
  action$.pipe(
    ofType(SuddenMeetingActionTypes.SUDDEN_MEETING_ENDED),
    switchMap(() => [new EnablePaddle(), new EndEvent()])
  );

export const suddenMeetingEpic$ = combineEpics(
  suddenMeetingStarted$,
  suddenMeetingEvent$,
  suddenMeetingTimer$,
  suddenMeetingEnded$
);
