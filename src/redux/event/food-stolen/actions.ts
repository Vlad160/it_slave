import { Action } from 'redux';

export enum FoodStolenActionTypes {
  FOOD_STOLEN_STARTED = '[Food stolen event] Food stolen event started',
  FOOD_STOLEN_ENDED = '[Food stolen event] Food stolen event ended',
  FIND_TRAITOR = '[Food stolen event] Find traitor',
  STAY_HUNGRY = '[Food stolen event] Stay hungry',
}

export class FoodStolenStarted implements Action {
  readonly type = FoodStolenActionTypes.FOOD_STOLEN_STARTED;
}

export class FoodStolenEnded implements Action {
  readonly type = FoodStolenActionTypes.FOOD_STOLEN_ENDED;
}

export class FindTraitor implements Action {
  readonly type = FoodStolenActionTypes.FIND_TRAITOR;
}

export class StayHungry implements Action {
  readonly type = FoodStolenActionTypes.STAY_HUNGRY;
}

export type FoodStolenActions =
  | FoodStolenStarted
  | FoodStolenEnded
  | FindTraitor
  | StayHungry;
