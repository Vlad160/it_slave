import { EventActions, EventActionTypes } from './actions';
import { EventDetails, PaddleEvent } from '../../types';
import { events } from './events';

export interface EventState {
  opened: boolean;
  eventsCount: number;
  event: EventDetails;
  events: PaddleEvent[];
}

export const initialState: EventState = {
  opened: false,
  event: null,
  eventsCount: 0,
  events: events,
};

export default function eventBoxReducer(
  state: EventState = initialState,
  action: EventActions
) {
  switch (action.type) {
    case EventActionTypes.OPEN_EVENT_BOX:
      return { ...state, opened: true, event: action.payload };
    case EventActionTypes.CLOSE_EVENT_BOX:
      return { ...state, event: null, opened: false };
    case EventActionTypes.EVENT_START:
      return { ...state, eventsCount: state.eventsCount + 1 };
    case EventActionTypes.EVENT_END:
      return { ...state, eventsCount: state.eventsCount - 1 };
    case EventActionTypes.SET_PRIORITY: {
      const { name, priority } = action.payload;
      const event = state.events.find(event => event.name === name);
      if (!event) {
        return state;
      }
      event.setPriority(priority);
      break;
    }
    case EventActionTypes.ADD_PRIORITY: {
      const { name, priority } = action.payload;
      const event = state.events.find(event => event.name === name);
      if (!event) {
        return state;
      }
      event.addPriority(priority);
      break;
    }
  }
  return state;
}
