import { race } from 'rxjs';
import { ActionsObservable, ofType } from 'redux-observable';
import { EventActionTypes } from './actions';
import { mapTo, take } from 'rxjs/operators';
import { Action } from 'redux';

export const waitForAnswer = <T extends Action>(
  actions$: ActionsObservable<T>
) =>
  race(
    actions$.pipe(
      ofType(EventActionTypes.SUBMIT_EVENT),
      mapTo(true),
      take(1)
    ),
    actions$.pipe(
      ofType(EventActionTypes.DENY_EVENT),
      mapTo(false),
      take(1)
    )
  ).pipe(take(1));
