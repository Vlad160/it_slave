import { Priority } from '../../types';
import {
  ChangePriority,
  CloseEventBox,
  EventActions,
  EventActionTypes,
  StartEvent,
  ToggleEventTimer,
} from './actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { EMPTY, interval, merge, Observable } from 'rxjs';
import {
  delay,
  distinctUntilKeyChanged,
  filter,
  map,
  mapTo,
  switchMap,
  switchMapTo,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { fromPayload } from '../redux-helper';
import {
  CommonActions,
  CommonActionTypes,
  DisablePaddle,
  EnablePaddle,
} from '../common';
import { firedEventEpic$ } from './fired/epic';
import { smokeBreakEpic$ } from './smoke-break/epics';
import { suddenMeetingEpic$ } from './sudden-meeting/epics';
import { beerEpic$ } from './beer/epics';
import { randomChoice } from '../../utils';
import { demotedEpic$ } from './demoted/epics';
import { AppState } from '../index';
import { foodStolenEpic$ } from './food-stolen/epics';
import { friendlyManagerEpic$ } from './friendly-manager/epics';
import { secondPaddleEpic$ } from './second-paddle/epics';
import { managerProtectsEpic$ } from './manager-protects/epics';
import { noChanceEpic$ } from './no-chance/epics';
import { getGameStarted, getIncome } from '../common/selectors';
import { getEvent, getEvents } from './selectors';
import { suddenManagerEpic$ } from './sudden-manager/epics';
import { message } from 'antd';

const RANDOM_EVENT_TIMER = 10 * 1000;
const DISABLE_EVENT_AFTER_START_TIME = 30 * 1000;
const EVENT_PROBABILITY = 0.3;
const MIN_INCOME_TO_GET_EVENT = 10;

const toggleEventTimer$ = (
  action$: ActionsObservable<CommonActions & EventActions>
): Observable<EventActions> =>
  merge(
    action$.pipe(
      ofType(CommonActionTypes.GAME_START, EventActionTypes.EVENT_END),
      map(() => true)
    ),
    action$.pipe(
      ofType(CommonActionTypes.GAME_END, EventActionTypes.EVENT_START),
      map(() => false)
    )
  ).pipe(map(active => new ToggleEventTimer({ active })));

const closeEventBoxOnEventEnd$ = (
  action$: ActionsObservable<EventActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.EVENT_END),
    mapTo(new CloseEventBox())
  );

const changePriority$ = (
  action$: ActionsObservable<EventActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.ADD_PRIORITY, EventActionTypes.SET_PRIORITY),
    map(fromPayload),
    withLatestFrom(state$),
    map(
      ([{ name }, state]) =>
        new ChangePriority({
          name,
          priority: getEvent(state, name).getPriority(),
        })
    )
  );

const randomEventTimer$ = (
  action$: ActionsObservable<EventActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.TOGGLE_EVENT_TIMER),
    map(fromPayload),
    distinctUntilKeyChanged('active'),
    switchMap(({ active }) => (!active ? EMPTY : interval(RANDOM_EVENT_TIMER))),
    withLatestFrom(state$),
    filter(
      ([, state]) =>
        Math.random() <= EVENT_PROBABILITY ||
        getIncome(state) < MIN_INCOME_TO_GET_EVENT
    ),
    map(([, state]) => {
      const possibleEvents = getEvents(state).filter(
        event => !event.isDisabled() && event.getPriority() > Priority.VERY_LOW
      );
      return randomChoice(possibleEvents);
    }),
    map(({ name }) => new StartEvent({ name }))
  );

const disableEventAfterStart$ = (
  action$: ActionsObservable<EventActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.EVENT_START),
    map(fromPayload),
    withLatestFrom(state$),
    tap(([{ name }, state]) => {
      const event = getEvents(state).find(event => event.name === name);
      if (!event) {
        return;
      }
      event.disable(DISABLE_EVENT_AFTER_START_TIME);
    }),
    switchMapTo(EMPTY)
  );

const togglePaddleOnEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<CommonActions> =>
  merge(
    action$.pipe(
      ofType(EventActionTypes.EVENT_START),
      mapTo(new DisablePaddle())
    ),
    action$.pipe(
      ofType(EventActionTypes.EVENT_END),
      mapTo(new EnablePaddle())
    )
  );

export const scheduleEvent$ = (
  action$: ActionsObservable<EventActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.SCHEDULE_EVENT),
    delay(10),
    map(fromPayload),
    withLatestFrom(state$),
    filter(([, state]) => getGameStarted(state)),
    map(([{ name }]) => new StartEvent({ name }))
  );

export const notifications$ = (
  action$: ActionsObservable<EventActions>
): Observable<EventActions> =>
  merge(
    action$.pipe(
      ofType(EventActionTypes.ADD_PRIORITY),
      map(fromPayload),
      map(
        ({ name, priority }) => `Шанс выпадения ${name} увеличен на ${priority}`
      )
    ),
    action$.pipe(
      ofType(EventActionTypes.SET_PRIORITY),
      map(fromPayload),
      map(
        ({ name, priority }) =>
          `Шанс выпадения ${name} теперь равен ${priority}`
      )
    )
  ).pipe(
    map(msg => message.info(msg)),
    switchMapTo(EMPTY)
  );

export const eventsEpic$ = combineEpics(
  toggleEventTimer$,
  randomEventTimer$,
  togglePaddleOnEvent$,
  disableEventAfterStart$,
  closeEventBoxOnEventEnd$,
  scheduleEvent$,
  changePriority$,
  notifications$,
  smokeBreakEpic$,
  firedEventEpic$,
  suddenMeetingEpic$,
  beerEpic$,
  demotedEpic$,
  foodStolenEpic$,
  friendlyManagerEpic$,
  secondPaddleEpic$,
  managerProtectsEpic$,
  noChanceEpic$,
  suddenManagerEpic$
);
