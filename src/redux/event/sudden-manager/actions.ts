import { Action } from 'redux';

export enum SuddenManagerActionTypes {
  SUDDEN_MANAGER_STARTED = '[Sudden manager event] Sudden manager event started',
  SUDDEN_MANAGER_ENDED = '[Sudden manager event] Sudden manager event ended',
}

export class SuddenManagerStarted implements Action {
  readonly type = SuddenManagerActionTypes.SUDDEN_MANAGER_STARTED;
}

export class SuddenManagerEnded implements Action {
  readonly type = SuddenManagerActionTypes.SUDDEN_MANAGER_ENDED;
}

export type SuddenManagerActions = SuddenManagerStarted | SuddenManagerEnded;
