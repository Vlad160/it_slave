import { PaddleEvent, Priority } from '../../../types';
import {
  AddPriority,
  EndEvent,
  EventActions,
  EventNames,
  OpenEventBox,
  ScheduleEvent,
} from '../actions';
import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import { Observable } from 'rxjs';
import { ofEventType } from '../../redux-helper';
import { map, mapTo, switchMap } from 'rxjs/operators';
import {
  SuddenManagerActions,
  SuddenManagerActionTypes,
  SuddenManagerStarted,
} from './actions';
import { waitForAnswer } from '../operators';
import { AddPassiveIncome, CommonActions } from '../../common';
import { SuddenMeetingEnded } from '../sudden-meeting/actions';

export const suddenManager = new PaddleEvent(
  EventNames.SUDDEN_MANAGER,
  Priority.SPECIAL,
  {
    title: 'Внезапно пришел погонщик',
    denyText:
      'Молча протянуть погонщику 0.5 л в надежде что он примет сей скромный дар',
    description:
      'Погонщик обнаружил, что вы тратите время не на пользу компании с веслом в руке, а сидите и чилите',
    submitText: 'Спохватиться и начать грести с двойной силой',
  }
);

const SUDDEN_MANAGER_CHANCE = 0.5;
const onSuddenManagerEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<SuddenManagerActions> =>
  action$.pipe(
    ofEventType(EventNames.SUDDEN_MANAGER),
    mapTo(new SuddenManagerStarted())
  );

const suddenManagerEventStarted$ = (
  action$: ActionsObservable<SuddenManagerActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(SuddenManagerActionTypes.SUDDEN_MANAGER_STARTED),
    mapTo(new OpenEventBox(suddenManager.details))
  );

const suddenManagerEvent$ = (
  action$: ActionsObservable<EventActions | SuddenManagerActions>
): Observable<EventActions | SuddenManagerActions | CommonActions> =>
  action$.pipe(
    ofType(SuddenManagerActionTypes.SUDDEN_MANAGER_STARTED),
    switchMap(() => waitForAnswer(action$)),
    map(result => {
      if (!result) {
        return new AddPassiveIncome({ value: 1, time: 100 * 1000 });
      }
      const prob = Math.random();
      return prob > SUDDEN_MANAGER_CHANCE
        ? new AddPriority({
            name: EventNames.FRIENDLY_MANAGER,
            priority: Priority.ABOVE_LOW,
          })
        : new ScheduleEvent({ name: EventNames.DEMOTED });
    }),
    switchMap((action: any) => [action, new SuddenMeetingEnded()])
  );

const suddenManagerEventEnded$ = (
  action$: ActionsObservable<SuddenManagerActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(SuddenManagerActionTypes.SUDDEN_MANAGER_ENDED),
    mapTo(new EndEvent())
  );

export const suddenManagerEpic$ = combineEpics(
  onSuddenManagerEvent$,
  suddenManagerEventStarted$,
  suddenManagerEvent$,
  suddenManagerEventEnded$
);
