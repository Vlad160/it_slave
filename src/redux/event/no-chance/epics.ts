import { PaddleEvent, Priority } from '../../../types';
import { EndEvent, EventActions, EventNames, OpenEventBox } from '../actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { Observable } from 'rxjs';
import { ofEventType } from '../../redux-helper';
import { map, mapTo, switchMap, withLatestFrom } from 'rxjs/operators';
import {
  NoChanceActions,
  NoChanceActionTypes,
  NoChanceEnded,
  NoChanceStarted,
} from './actions';
import { waitForAnswer } from '../operators';
import { AddIncome, AddPassiveIncome, CommonActions } from '../../common';
import { getCPS } from '../../common/selectors';
import { AppState } from '../../index';

export const noChanceEvent = new PaddleEvent(
  EventNames.NO_CHANCE,
  Priority.SPECIAL,
  {
    title: 'Попытки оказались тщетными',
    denyText: 'Да ёпт! Ай, и так погребу',
    description:
      'Все оказались безразличными к вашим стенаниям и дали понять, что работа это работа, тут надо грести и не мешать другим. Также вам пожелали хорошего дня.',
    submitText: null,
  }
);

const onNoChanceEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<NoChanceActions> =>
  action$.pipe(
    ofEventType(EventNames.NO_CHANCE),
    mapTo(new NoChanceStarted())
  );

const noChanceEventStarted$ = (
  action$: ActionsObservable<NoChanceActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(NoChanceActionTypes.NO_CHANCE_STARTED),
    mapTo(new OpenEventBox(noChanceEvent.details))
  );

const noChanceEvent$ = (
  action$: ActionsObservable<NoChanceActions>,
  state$: StateObservable<AppState>
): Observable<CommonActions | NoChanceActions> =>
  action$.pipe(
    ofType(NoChanceActionTypes.NO_CHANCE_STARTED),
    switchMap(() => waitForAnswer(action$)),
    withLatestFrom(state$),
    map(([, state]) => {
      const cps = getCPS(state);
      return cps === 1
        ? new AddIncome({ value: -100 })
        : new AddPassiveIncome({ value: -1 });
    }),
    switchMap(action => [action, new NoChanceEnded()])
  );

const noChanceEventEnded$ = (
  action$: ActionsObservable<NoChanceActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(NoChanceActionTypes.NO_CHANCE_ENDED),
    mapTo(new EndEvent())
  );

export const noChanceEpic$ = combineEpics(
  onNoChanceEvent$,
  noChanceEventStarted$,
  noChanceEvent$,
  noChanceEventEnded$
);
