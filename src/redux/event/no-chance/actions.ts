import { Action } from 'redux';

export enum NoChanceActionTypes {
  NO_CHANCE_STARTED = '[No chance event] No chance event started',
  NO_CHANCE_ENDED = '[No chance event] No chance event ended',
}

export class NoChanceStarted implements Action {
  readonly type = NoChanceActionTypes.NO_CHANCE_STARTED;
}

export class NoChanceEnded implements Action {
  readonly type = NoChanceActionTypes.NO_CHANCE_ENDED;
}

export type NoChanceActions = NoChanceStarted | NoChanceEnded;
