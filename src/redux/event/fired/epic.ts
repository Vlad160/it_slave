import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import {
  CommonActions,
  CommonActionTypes,
  GameEnd,
  ToggleIdleTimer,
} from '../../common';
import {
  EndEvent,
  EventActions,
  EventNames,
  OpenEventBox,
  ScheduleEvent,
} from '../actions';
import { concat, EMPTY, merge, Observable, of, timer } from 'rxjs';
import { map, mapTo, switchMap } from 'rxjs/operators';
import { fromPayload, ofEventType } from '../../redux-helper';
import { PaddleEvent, Priority } from '../../../types';
import { waitForAnswer } from '../operators';
import {
  FiredActions,
  FiredActionTypes,
  FiredEnded,
  FiredStarted,
} from './actions';

export const firedEvent = new PaddleEvent(EventNames.FIRED, Priority.SPECIAL, {
  title: 'УВОЛЕН',
  denyText: null,
  description:
    'Вы совершенно бездарный гребец, вы даже с веслом не управились. Вас уволили. Всего хорошего и удачного вам дня. Спасибо за ваши старания',
  submitText: 'Молча как терпила всё стерпеть',
});

const firedStarted$ = (
  action$: ActionsObservable<EventActions>
): Observable<FiredActions> =>
  action$.pipe(
    ofEventType(EventNames.FIRED),
    mapTo(new FiredStarted())
  );

export const FIRED_EVENT_TIMER = 300 * 1000;

export const toggleFiredEventTimer$ = (
  action$: ActionsObservable<CommonActions | EventActions>
): Observable<CommonActions> =>
  merge(
    action$.pipe(
      ofType(CommonActionTypes.PADDLE_CLICK, CommonActionTypes.GAME_START),
      mapTo(true)
    ),
    action$.pipe(
      ofType(CommonActionTypes.GAME_END),
      mapTo(false)
    )
  ).pipe(map(active => new ToggleIdleTimer({ active })));

export const firedEventTimer$ = (
  action$: ActionsObservable<CommonActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(CommonActionTypes.TOGGLE_IDLE_TIMER),
    map(fromPayload),
    switchMap(({ active }) => {
      return !active
        ? EMPTY
        : timer(FIRED_EVENT_TIMER).pipe(
            mapTo(new ScheduleEvent({ name: EventNames.FIRED }))
          );
    })
  );

export const firedEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<EventActions | CommonActions> =>
  action$.pipe(
    ofType(FiredActionTypes.FIRED_STARTED),
    mapTo(new OpenEventBox(firedEvent.details))
  );

export const firedEventEnded$ = (
  action$: ActionsObservable<FiredActions>
): Observable<EventActions | CommonActions | FiredActions> =>
  action$.pipe(
    ofType(FiredActionTypes.FIRED_STARTED),
    switchMap(() =>
      concat(
        waitForAnswer(action$).pipe(map(() => new FiredEnded())),
        of(new EndEvent()),
        of(new GameEnd())
      )
    )
  );

export const firedEventEpic$ = combineEpics(
  firedStarted$,
  toggleFiredEventTimer$,
  firedEventTimer$,
  firedEvent$,
  firedEventEnded$
);
