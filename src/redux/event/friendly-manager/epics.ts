import { PaddleEvent, Priority } from '../../../types';
import {
  EndEvent,
  EventActions,
  EventActionTypes,
  EventNames,
  OpenEventBox,
} from '../actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { Observable, timer } from 'rxjs';
import { fromPayload, ofEventType } from '../../redux-helper';
import {
  filter,
  map,
  mapTo,
  mergeMap,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  FriendlyManagerActions,
  FriendlyManagerActionTypes,
  FriendlyManagerEnded,
  FriendlyManagerStarted,
} from './actions';
import { AddIncome, CommonActions } from '../../common';
import { AppState } from '../../index';
import { getEvent } from '../selectors';
import { getIncomeToNextRole } from '../../common/operators';
import {
  getCurrentRole,
  getGameStarted,
  getIncome,
  getRoles,
} from '../../common/selectors';
import { DemotedActions } from '../demoted/actions';

const PRIORITY_MULTIPLIER = 0.025;
const FRIENDLY_MANAGER_TIMER = 30 * 1000;

export const friendlyManagerEvent = new PaddleEvent(
  EventNames.FRIENDLY_MANAGER,
  Priority.NONE,
  {
    title: 'Законтачился с погонщиком',
    denyText: null,
    description: `Хоть вы всего навсего просто очередной гребец для погонщика, но он вас знает и относится с пониманием,
     ваши труды не прошли даром и он решил замолвить за вас словечко в вопросе вашего повышения.
      Хоть это и все в его корыстных целях, тем не менее вам тоже приятно.`,
    submitText: 'Ура повысили!',
  }
);

const onFriendlyManager$ = (
  action$: ActionsObservable<EventActions>
): Observable<FriendlyManagerActions> =>
  action$.pipe(
    ofEventType(EventNames.FRIENDLY_MANAGER),
    mapTo(new FriendlyManagerStarted())
  );

const friendlyManagerEventStarted$ = (
  action$: ActionsObservable<FriendlyManagerActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(FriendlyManagerActionTypes.FRIENDLY_MANAGER_STARTED),
    mapTo(new OpenEventBox(friendlyManagerEvent.details))
  );

const friendlyManagerEvent$ = (
  action$: ActionsObservable<EventActions>,
  state$: StateObservable<AppState>
): Observable<CommonActions | FriendlyManagerActions> =>
  action$.pipe(
    ofType(EventActionTypes.CHANGE_PRIORITY),
    map(fromPayload),
    filter(({ name }) => name === EventNames.FRIENDLY_MANAGER),
    filter(({ priority }) => priority >= Priority.VERY_LOW),
    mergeMap(() =>
      timer(FRIENDLY_MANAGER_TIMER).pipe(
        withLatestFrom(state$),
        filter(([, state]) => getGameStarted(state)),
        map(([, state]) => {
          const event = getEvent(state, EventNames.FRIENDLY_MANAGER);
          if (!event) {
            return null;
          }
          const prob = PRIORITY_MULTIPLIER * event.getPriority();
          const shouldPromote = prob >= Math.random();
          if (!shouldPromote) {
            return null;
          }
          const currentRole = getCurrentRole(state);
          const roles = getRoles(state);
          const currentIncome = getIncome(state);
          return new AddIncome({
            value: getIncomeToNextRole(roles, currentRole, currentIncome),
          });
        }),
        filter<AddIncome>(Boolean),
        switchMap(action => [action, new FriendlyManagerEnded()])
      )
    )
  );

const friendlyManagerEventEnded$ = (
  action$: ActionsObservable<DemotedActions>
): Observable<EventActions | CommonActions | DemotedActions> =>
  action$.pipe(
    ofType(FriendlyManagerActionTypes.FRIENDLY_MANAGER_ENDED),
    mapTo(new EndEvent())
  );

export const friendlyManagerEpic$ = combineEpics(
  onFriendlyManager$,
  friendlyManagerEventStarted$,
  friendlyManagerEvent$,
  friendlyManagerEventEnded$
);
