import { Action } from 'redux';

export enum FriendlyManagerActionTypes {
  FRIENDLY_MANAGER_STARTED = '[Friendly manager event] Friendly manager event started',
  FRIENDLY_MANAGER_ENDED = '[Friendly manager event] Friendly manager event ended',
}

export class FriendlyManagerStarted implements Action {
  readonly type = FriendlyManagerActionTypes.FRIENDLY_MANAGER_STARTED;
}

export class FriendlyManagerEnded implements Action {
  readonly type = FriendlyManagerActionTypes.FRIENDLY_MANAGER_ENDED;
}

export type FriendlyManagerActions =
  | FriendlyManagerStarted
  | FriendlyManagerEnded;
