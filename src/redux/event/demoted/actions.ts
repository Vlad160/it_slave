import { Action } from 'redux';

export enum DemotedActionTypes {
  DEMOTED_STARTED = '[Demoted event] Demoted event started',
  DEMOTED_ENDED = '[Demoted event] Demoted event ended',
}

export class DemotedStarted implements Action {
  readonly type = DemotedActionTypes.DEMOTED_STARTED;
}

export class DemotedEnded implements Action {
  readonly type = DemotedActionTypes.DEMOTED_ENDED;
}

export type DemotedActions = DemotedStarted | DemotedEnded;
