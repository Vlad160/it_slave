import { Action } from 'redux';
import { LogLevel, Role } from '../../types';

export enum CommonActionTypes {
  APP_INIT = '[Common] App INIT',
  ADD_INCOME = '[Common] Add income',
  PADDLE_CLICK = '[Common] Paddle click',
  AUTO_PADDLE_TOGGLE = '[Common] Auto paddle toggle',
  RESET_INCOME = '[Common] Reset income',
  TOGGLE_PASSIVE_INCOME = '[Common] Activate passive income',
  CALCULATE_PROGRESS = '[Common] Calculate progress',
  SET_PROGRESS = '[Common] Set progress',
  GAME_START = '[Common] Game start',
  GAME_END = '[Common] Game end',
  TOGGLE_IDLE_TIMER = '[Common] Toggle idle timer',
  ADD_PASSIVE_INCOME = '[Common] Add passive income',
  DISABLE_PADDLE = '[Common] Disable paddle',
  ENABLE_PADDLE = '[Common] Enable paddle',
  SET_ROLE = '[Common] Set role',
}

export class AppInit implements Action {
  readonly type = CommonActionTypes.APP_INIT;
}

export class AddIncome implements Action {
  readonly type = CommonActionTypes.ADD_INCOME;
  constructor(public payload: { value: number; log?: LogLevel }) {}
}

export class ResetIncome implements Action {
  readonly type = CommonActionTypes.RESET_INCOME;
}

export class TogglePassiveIncome implements Action {
  readonly type = CommonActionTypes.TOGGLE_PASSIVE_INCOME;
  constructor(public payload: { active: boolean }) {}
}

export class CalculateProgress implements Action {
  readonly type = CommonActionTypes.CALCULATE_PROGRESS;
}

export class SetProgress implements Action {
  readonly type = CommonActionTypes.SET_PROGRESS;
  constructor(public payload: Role[]) {}
}

export class GameStart implements Action {
  readonly type = CommonActionTypes.GAME_START;
}

export class GameEnd implements Action {
  readonly type = CommonActionTypes.GAME_END;
}

export class ToggleIdleTimer implements Action {
  readonly type = CommonActionTypes.TOGGLE_IDLE_TIMER;
  constructor(public payload: { active: boolean }) {}
}

export class AddPassiveIncome implements Action {
  readonly type = CommonActionTypes.ADD_PASSIVE_INCOME;
  constructor(
    public payload: { time?: number; value: number; log?: LogLevel }
  ) {}
}

export class DisablePaddle implements Action {
  readonly type = CommonActionTypes.DISABLE_PADDLE;
}

export class EnablePaddle implements Action {
  readonly type = CommonActionTypes.ENABLE_PADDLE;
}

export class PaddleClick implements Action {
  readonly type = CommonActionTypes.PADDLE_CLICK;
}

export class ToggleAutoPaddle implements Action {
  readonly type = CommonActionTypes.AUTO_PADDLE_TOGGLE;
  constructor(public payload: { active: boolean }) {}
}

export class SetRole implements Action {
  readonly type = CommonActionTypes.SET_ROLE;
  constructor(public payload: { role: Role }) {}
}

export type CommonActions =
  | AddIncome
  | TogglePassiveIncome
  | CalculateProgress
  | SetProgress
  | GameStart
  | GameEnd
  | ToggleIdleTimer
  | ResetIncome
  | AddPassiveIncome
  | DisablePaddle
  | EnablePaddle
  | PaddleClick
  | ToggleAutoPaddle
  | SetRole;
