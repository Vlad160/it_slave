/**
 *
 * @param roles
 * @param currentRole
 * @param at number lover/above roles. -2 meas role lower than current in 1 position; +3 means upper that current in 3 positions
 */
import { Role } from '../../types';

export const getRoleAtDirection = (
  roles: Role[],
  currentRole: Role,
  at: number
) => {
  const sorted = roles.sort((a, b) => (a.amount - b.amount) * at);
  const len = sorted.length;
  const currentIndex = roles.findIndex(role => role.id === currentRole.id);
  const role = roles[currentIndex + Math.abs(at)];
  if (!role) {
    return at < 0 ? roles[len - 1] : roles[0];
  }
  return role;
};

export const getIncomeToRole = (
  roles: Role[],
  currentRole: Role,
  currentIncome: number,
  at: number
) => {
  const role = getRoleAtDirection(roles.slice(), currentRole, at);
  return role.amount - currentIncome;
};

export const getIncomeToNextRole = (
  roles: Role[],
  currentRole: Role,
  currentIncome: number
) => getIncomeToRole(roles, currentRole, currentIncome, 1);

export const getIncomeToPrevRole = (
  roles: Role[],
  currentRole: Role,
  currentIncome: number
) => getIncomeToRole(roles, currentRole, currentIncome, -1);
