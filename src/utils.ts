export const zeroOrPositive = (value: number) => (value < 0 ? 0 : value);

export const randomChoice = <T>(choices: T[]): T => {
  const index = Math.floor(Math.random() * choices.length);
  return choices[index];
};

export const defaultValueIfBelow = (
  value: number,
  bound: number,
  defaultValue: number
) => (value < bound ? defaultValue : value);

//check if value is primitive
const isPrimitive = obj => obj !== Object(obj);

export const deepEqual = (obj1, obj2) => {
  if (obj1 === obj2)
    // it's just the same object. No need to compare.
    return true;

  if (isPrimitive(obj1) && isPrimitive(obj2))
    // compare primitives
    return obj1 === obj2;

  if (Object.keys(obj1).length !== Object.keys(obj2).length) return false;

  // compare objects with same number of keys
  for (let key in obj1) {
    if (!(key in obj2)) return false; //other object doesn't have this prop
    if (!deepEqual(obj1[key], obj2[key])) return false;
  }

  return true;
};

export const getRandomInt = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const uuidv4 = () =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = (Math.random() * 16) | 0,
      v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
