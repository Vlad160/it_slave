import React from "react";
import './footer.scss';

export const Footer = () =>
    <footer className="footer-component">
        <div className="footer-container">
            <span className="footer-disclaimer">ЛЮБЫЕ СОВПАДЕНИЯ С РЕАЛЬНЫМИ СОБЫТИЯМИ СЛУЧАЙНЫ</span>
        </div>
    </footer>;

export default Footer;