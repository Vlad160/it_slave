import React, { Component, createRef } from 'react';
import './overlay.scss';
import { getRandomInt, uuidv4 } from '../../utils';
import { Observable, Subscription } from 'rxjs';

interface OverlayState {
  elements: OverlayElement[];
}
interface OverlayProps {
  addElement$: Observable<string | number>;
}
interface OverlayElement {
  id: string;
  top: string;
  left: string;
  value: string | number;
  createdAt: number;
}

const REMOVE_ELEMENT_AFTER = 2000;

export class Overlay extends Component<OverlayProps, OverlayState> {
  ref = createRef<HTMLDivElement>();
  subscription: Subscription;
  intervalId: number;

  constructor(props) {
    super(props);
    this.state = { elements: [] };
  }

  componentDidMount(): void {
    this.subscription = this.props.addElement$.subscribe(
      this.addElement.bind(this)
    );
    this.intervalId = (setInterval(
      this.cleanUp.bind(this),
      1000
    ) as unknown) as number;
  }

  componentWillUnmount(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    clearInterval(this.intervalId);
  }

  addElement(value) {
    const id = uuidv4();
    const { offsetHeight, offsetWidth } = this.ref.current;
    const createdAt = Date.now();
    const element = {
      top: `${getRandomInt(0, offsetHeight)}px`,
      left: `${getRandomInt(0, offsetWidth)}px`,
      id,
      value,
      createdAt,
    };
    this.setState(prevState => ({
      elements: [...prevState.elements, element],
    }));
  }

  cleanUp() {
    this.setState(state => {
      const now = Date.now();
      return {
        elements: state.elements.filter(({ createdAt }) => {
          return now - createdAt < REMOVE_ELEMENT_AFTER;
        }),
      };
    });
  }

  render() {
    return (
      <div ref={this.ref} className="overlay-container">
        {this.state.elements.map(({ top, left, value, id }) => (
          <span
            className="overlay-item"
            key={id}
            style={{
              top,
              left,
            }}
          >
            {value}
          </span>
        ))}
      </div>
    );
  }
}

export default Overlay;
